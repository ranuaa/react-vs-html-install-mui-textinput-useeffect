import { Box, Typography } from "@mui/material";
import React from "react";

const List = ({ user }) => {
    return (
        <Box>
            {user?.length !== 0 ?
                <Box>
                    {user.map((usr) => {
                        return (
                            <Box sx={{ width: "100vw", display: "flex", border: "1px solid black", padding: "10px" }}>
                                <Box sx={{ width: "50%" }}>
                                    <Typography>
                                        {usr.name}
                                    </Typography>
                                    <Typography>
                                        {usr.hobby}
                                    </Typography>
                                </Box>
                                <Box sx={{ width: "50%", display: "flex", justifyContent: "center", alignItems: "center" }} >{usr.address}</Box>
                            </Box>
                        )
                    })}
                </Box>
                :
                <Box sx={{ width: "100%", height: "100vh", display: "flex", justifyContent: "center", alignItems: "center", flexDirection: "column" }}>
                    <Box sx={{ width: "125px", height: "125px", border: "25px solid #f7b531", borderRadius: "50%" }} />
                    <Typography color="#ff5757" sx={{ fontSize: "60px", fontWeight: "bold", fontStyle: 'italic' }}>
                        User
                    </Typography>
                </Box>
            }
        </Box>
    );
};

export default List;
