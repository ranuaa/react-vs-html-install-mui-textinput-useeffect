import React, { useEffect, useState } from "react";
import List from "../Components/List";
import Navbar from "../Components/Navbar";
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import { TextField } from "@mui/material";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
    display: "flex",
    flexDirection: "column",
};



const user = []

const LandingPage = () => {
    const [name, setName] = useState("")
    const [address, setAddress] = useState("")
    const [hobby, setHobby] = useState("")
    const [open, setOpen] = useState(false);
    const handleOpen = () => {
        setOpen(true)
        console.log("clicked")
    };
    const handleClose = () => setOpen(false);

    const handleSave = () => {
        user.push({ name, address, hobby })
        setName("")
        setAddress("")
        setHobby("")
        handleClose()
    }

    useEffect(() => {

    }, [user]);

    return (
        <>
            <Navbar func={handleOpen} />
            <List user={user} />
            <div>
                <Modal
                    open={open}
                    onClose={handleClose}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                >
                    <Box sx={style}>
                        <Typography textAlign="center" id="modal-modal-title" variant="h6" component="h2">
                            Add User
                        </Typography>
                        <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                            Name
                        </Typography>
                        <TextField onChange={(e) => setName(e.target.value)} />
                        <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                            Address
                        </Typography>
                        <TextField onChange={(e) => setAddress(e.target.value)} />
                        <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                            Hobby
                        </Typography>
                        <TextField onChange={(e) => setHobby(e.target.value)} />
                        <Box sx={{ display: "flex", justifyContent: "center" }}>
                            <Button sx={{ backgroundColor: "#00c2cb", color: "black", width: "30%", marginTop: "2rem" }} onClick={handleSave}>Save</Button>
                        </Box>
                    </Box>
                </Modal>
            </div>
        </>
    );
};

export default LandingPage;
